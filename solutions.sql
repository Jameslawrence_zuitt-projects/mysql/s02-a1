CREATE DATABASE blog_db;

CREATE TABLE Posts ( 
	id INT, 
	author_id INT,
	title VARCHAR(500),
	content VARCHAR(500),
	datetime_posted DATETIME,
	PRIMARY KEY(id),
	FOREIGN KEY(author_id) REFERENCES Author(id)
);

CREATE TABLE Post_Comments(
	id INT,
	post_id INT,
	user_id INT,
	content VARCHAR(500),
	datetime_commented DATETIME,
	PRIMARY KEY(id),
	FOREIGN KEY(post_id) REFERENCES Posts(id),
	FOREIGN KEY(user_id) REFERENCES Users(id)
);

CREATE TABLE Users(
	id INT,
	email VARCHAR(100),
	password VARCHAR(300),
	datetime_created DATETIME,
	PRIMARY KEY(id),
);

CREATE TABLE post_likes( 
	id INT, 
	post_id INT, 
	user_id INT, 
	datetime_liked DATETIME, 
	PRIMARY KEY(id), 
	FOREIGN KEY(post_id) REFERENCES Posts(id), 
	FOREIGN KEY(user_id) REFERENCES Users(id)
);

